#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# Joe - jlopez.mail@gmail.com

from time import strftime,localtime
import MySQLdb, sys
import os
import time
import subprocess

# INICIALIZACION
llamados=0
inicio=830
fin=2030
trunk_celulares="Telular1-Out"
#trunk_red="Telular2-Out"
#trunk_celulares="iplan"
trunk_red="iplan"
#trunk_celulares_prefix="011"
#trunk_red_prefix="011"
trunk_celulares_prefix=""
trunk_red_prefix=""

# Opciones de primero : celular, red
primero="red"

# CONFIGURACION
dummy=1
if len(sys.argv)>1:
	if sys.argv[1]=="start":
		dummy=0

teldummy=1540727071
diroutgoing="/var/spool/asterisk/outgoing/"
dirapp="/root/at-5000"
dirwww="/var/www/html"

# -------------------------------------------------
if dummy==1:
	print "CORRIENDO EN MODO DUMMIE"
	diroutgoing="/var/spool/asterisk/probandollamadas/"

def enproceso(extension):
	if len(os.listdir(diroutgoing)) > 0:
		if extension==1101:
			extension_enproceso=1152
		else:
			extension_enproceso=extension
		os.system("grep 'Extension: %s' %s* | wc -l > %s/enproceso" % (extension_enproceso,diroutgoing,dirapp))
		inp=open("%s/enproceso" % dirapp,"r")
		for linea in inp.readlines():
			enproceso=int(linea)
		inp.close()
		os.system("rm %s/enproceso" % dirapp)
		return enproceso
	else:
		return 0

def agentesconectados(extension):
	
	#command="/usr/sbin/asterisk -rx 'queue show %s' | grep 'dynamic' | grep -v 'Unavailable' -c > %s/cantidadagentes" % (extension,dirapp)
	#print command
	os.system("/usr/sbin/asterisk -rx 'queue show %s' | grep 'dynamic' | grep -v 'Unavailable' -c > %s/cantidadagentes" % (extension,dirapp))
	inp=open("%s/cantidadagentes" % dirapp,"r")
	for linea in inp.readlines():
		agentes=int(linea)
	inp.close()
	#exit()
	os.system("rm %s/cantidadagentes" % dirapp)
	return agentes

def agenteslibres(extension):
	os.system("/usr/sbin/asterisk -rx 'queue show %s' | grep 'Not in use' -c > %s/agenteslibres" % (extension,dirapp))
	inp=open("%s/agenteslibres" % dirapp,"r")
	for linea in inp.readlines():
		agentes=int(linea)
	inp.close()
	os.system("rm %s/agenteslibres" % dirapp)
	return agentes

def agentesocupados(extension):
	os.system("/usr/sbin/asterisk -rx 'queue show %s' | grep 'In use' -c > %s/agentesocupados" % (extension,dirapp))
	inp=open("%s/agentesocupados" % dirapp,"r")
	for linea in inp.readlines():
		agentes=int(linea)
	inp.close()
	os.system("rm %s/agentesocupados" % dirapp)
	return agentes

if dummy==1:
	os.system("rm %s* " % diroutgoing)
while True:

	try:
    		db = MySQLdb.connect(host='db.siap',user='siap',passwd='tr0nch4',db='siap_coolskin')
 		session = db.cursor()
	except MySQLdb.Error, e:
        	print e
	        sys.exit(0)

	hora=int(strftime("%H%M",localtime()))
	ts=strftime("%D %T %a",localtime())

	session.execute("select valor from settings where llave='at5000_exigencia'")
	exigencia=session.fetchone()

	html="<meta http-equiv=refresh content=1><body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0><table border=0 width=100% cellpadding=0 cellspacing=1><tr align=center><td>Actualizacion</td><td>Extension</td><td>Agentes</td><td>Libres</td><td>Hablando</td><td>Exigencia</td><td>Discando</td><td>Llamadas Realizadas</td><td>Base Disponible</td></tr>"

	logfile=open("%s/logs/at5000-%s.log" % (dirapp,strftime("%Y%m%d")),"a")

	session.execute("""select extension,count(*) as base from at5000 where llamar<=date_FORMAT(now(),"%Y%m%d") and llamado=0 group by 1""")
	rstcolas=session.fetchall()
	for extension,base in rstcolas:
		disponibles=agentesconectados(extension)*int(exigencia[0])/100 # Conectados y su porcentaje
		disponibles=disponibles-enproceso(extension) # A llamar

		#print "Conectados: %s - En Proceso %s - Exigencia %s - Llamar a %i clientes" % (agentesconectados(extension),enproceso(extension),exigencia[0],(agentesconectados(extension)*int(exigencia[0])/100))
		print "Conectados: %s - En Proceso %s - Exigencia %s - Llamar a %i clientes" % (agentesconectados(extension),enproceso(extension),exigencia[0],disponibles)

		#log="%s | Extension %s - Agentes Conectados %s Libres %s Hablando %s Exigencia %s%% Llamadas en proceso %s Llamadas realizadas %s Base Disponible %s\n" % (ts,extension,agentesconectados(extension),agenteslibres(extension),agentesocupados(extension),exigencia[0],enproceso(extension),llamados,base)
		#if dummy==1:
		#	print log
		#logfile.write(log)
		html+="<tr align=center bgcolor=darkgrey><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s%%</td><td>%s</td><td>%s</td><td>%s</td></tr>" % (ts,extension,agentesconectados(extension),agenteslibres(extension),agentesocupados(extension),exigencia[0],enproceso(extension)-agentesocupados(extension),llamados,base)
		if hora >= inicio and hora <= fin and localtime()[6] >= 0 and localtime()[6]<=4:
			if disponibles>0:
				sql="""select dpclinumero,dpapellido,dpnombres,dpptelefono,dppcelular,id from datospersonales inner join at5000 on cliente=dpclinumero where llamar<=date_FORMAT(now(),"%%Y%%m%%d") and extension=%s and llamado=false limit %s""" % (extension,disponibles)
				print sql
				session.execute(sql)
				recordset = session.fetchall()
				for dpclinumero,dpapellido,dpnombres,dpptelefono,dppcelular,id in recordset:
					if primero=="celular":
						if dppcelular<>"":
							telefono=dppcelular
							telefono=trunk_celulares_prefix+telefono
							canal=trunk_celulares
						else:
							if dpptelefono<>"":
								telefono=dpptelefono
								telefono=trunk_red_prefix+telefono
								canal=trunk_red
					else:
						if dpptelefono<>"":
							telefono=dpptelefono
							telefono=trunk_red_prefix+telefono
							canal=trunk_red
						else:
							if dppcelular<>"":
								telefono=dppcelular
								telefono=trunk_celulares_prefix+telefono
								canal=trunk_celulares
		
					if dummy==1:
						telefono=teldummy
						extension=6000
					if telefono<>"":
						call=""
						call+="Channel: SIP/%s/%s\n" % (canal,telefono)
						call+="MaxRetries: 0\n"
						call+="RetryTime: 300\n"
						call+="WaitTime: 20\n"
						call+="Context: from-internal\n"
						if extension == 1101:
							call+="Extension: 1152\n"
						else:
							call+="Extension: %s\n" % extension
						call+="Priority: 1\n"
						call+="CallerID: '%s-%s %s' <%s>\n" % (dpclinumero,dpnombres,dpapellido,telefono)
						call+="Archive: Yes\n"
						f=open("%s%s.call"%(diroutgoing,dpclinumero),"w")
						f.write(call)
		                	        f.close()
						os.system("chmod +x %s%s.call"%(diroutgoing,dpclinumero))
						if dummy==0:
							session.execute("update at5000 set llamado=true,timestamp=now() where id=%s" % id)
							db.commit()
						log="%s Extension %s Trunk %s\tTelefono %s \tCliente %s (%s %s)\n" %(ts,extension,canal,telefono,dpclinumero,dpnombres,dpapellido)
						#log+="\n"
						if dummy==1:
							print log
						logfile.write(log)
						llamados=llamados+1
		else:
			log="*"*40+"FUERA DE HORARIO"+"*"*40
			log+="\n"
			if dummy==1:
				print log
			#logfile.write(log)
	logfile.close()
	html+="</table>"
	fhtml=open("%s/estado.html" % dirwww,"w")
	fhtml.write(html)
	fhtml.close()
	time.sleep(1)
	
	db.close()
